/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	theme: {
		fontFamily: {
			body: ['Akzidenz-Grotesk']
		},
		extend: {
			colors: {
				black: '#1B1B1B',
				purple: '#293894',
				gfgray: '#CFD1C7',
				gfgray2: '#DFE0D9',
				gfflint: '#6B6B65',
				red: '#E94235'
			}
		}
	},
	variants: {},
	plugins: []
}
